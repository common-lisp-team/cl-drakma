cl-drakma (2.0.10-1) unstable; urgency=medium

  * New upstream version 2.0.10
  * Added myself as uploader
  * Updated Standards-Version, no changes

 -- Peter Van Eynde <pvaneynd@debian.org>  Sat, 30 Nov 2024 11:12:05 +0100

cl-drakma (2.0.8-2) unstable; urgency=medium

  [ Sébastien Villemot ]
  * Remove myself from Uploaders

 -- Peter Van Eynde <pvaneynd@debian.org>  Sat, 30 Nov 2024 11:11:12 +0100

cl-drakma (2.0.8-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Sébastien Villemot ]
  * New upstream release
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.
  * disable-null-badssl-test.patch: drop patch, no longer needed

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 02 Nov 2021 17:17:42 +0100

cl-drakma (2.0.7-1) unstable; urgency=medium

  * New upstream release
  * Move under Debian Common Lisp Team maintenance. Thanks to Dimitri Fontaine
    for his work on this package.
  * Add debian/watch
  * Remove Build-Depends on dh-lisp
  * Bump to S-V 4.5.0
  * Update Homepage
  * Update d/copyright
  * Update Vcs-* fields for move to salsa.
  * Add Rules-Requires-Root: no
  * Mark as M-A foreign
  * Convert d/rules to dh
  * Rebuild doc at built time. Consequently, add Build-Depends on xsltproc
  * Downgrade cl-fiveam to a Recommends, since it is only needed for the
    testsuite
  * Add an autopkgtest that runs the testsuite under sbcl and clisp
  * disable-null-badssl-test.patch: new patch, disables a failing test

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 11 Apr 2020 19:34:37 +0200

cl-drakma (2.0.4-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Wed, 16 May 2018 09:48:21 +0000

cl-drakma (2.0.3-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Thu, 06 Jul 2017 16:28:53 +0300

cl-drakma (2.0.2-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Mon, 02 Nov 2015 20:33:41 +0300

cl-drakma (2.0.1-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Wed, 02 Sep 2015 16:21:48 +0300

cl-drakma (1.3.15-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Wed, 15 Jul 2015 16:44:50 +0300

cl-drakma (1.3.13-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Sat, 02 May 2015 17:01:30 +0300

cl-drakma (1.3.11-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Thu, 18 Dec 2014 18:13:42 +0300

cl-drakma (1.3.10-2) unstable; urgency=medium

  * Fix dependencies, that changed in Quicklisp release 2014-10-06

 -- Dimitri Fontaine <dim@tapoueh.org>  Tue, 14 Oct 2014 02:21:11 +0400

cl-drakma (1.3.10-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Fri, 10 Oct 2014 14:02:43 +0400

cl-drakma (1.3.9-2) unstable; urgency=medium

  * Drakma depends on cl+ssl

 -- Dimitri Fontaine <dim@tapoueh.org>  Mon, 25 Aug 2014 20:09:06 +0400

cl-drakma (1.3.9-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Mon, 04 Aug 2014 23:46:28 +0400
