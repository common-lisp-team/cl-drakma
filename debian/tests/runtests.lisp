(require "asdf")

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "drakma")
  (asdf:load-system "drakma-test"))

;; Can't use ASDF:TEST-SYSTEM, its return value is meaningless
(let ((results (5am:run :drakma)))
  (5am:explain! results)
  (unless (5am:results-status results)
    (uiop:quit 1)))
